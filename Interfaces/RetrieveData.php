<?php

namespace Interfaces;


interface RetrieveData
{
    /**
     * Retrieve Data From Data Store
     *
     *
     * @param $handler : Database or File Handler
     * @param $condition : which data that you want to retrieve
     * @return mixed
     */
    public function retrieveData($handler , $condition);
}