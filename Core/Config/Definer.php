<?php
namespace Core\Config;

/*
 * Define All Basic Constants
 * and Define Developer's Constants
 * */
class Definer
{
    public function __construct()
    {
        //Define the Base Directory
        $baseDir = $_SERVER['SCRIPT_FILENAME'];
        $baseDir = str_replace('index.php' , '' , $baseDir);

        define('BASE_DIR' , $baseDir);

        //Define the Base URL
        $baseUrl = $_SERVER['SCRIPT_NAME'];
        $baseUrl = str_replace('index.php' , '' , $baseUrl);

        define('BASE_URL' , $baseUrl);

        //Define Developer's Constants
        $constFile = $baseDir.".const";

        $consts = fopen($constFile , 'r+');
        while(!feof($consts)){
            $const = fgets($consts);

            //Ignore Comment and empty lines
            $const = trim($const);
            if($const != "" and $const[0] != "/"){
                $const = explode("=" , $const);

                define($const[0] , $const[1]);
            }
        }
    }
}